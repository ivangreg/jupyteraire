# Jupyter Aire

Author: Ivan Gregoretti, PhD.

Jupyter Aire is a JupyterLab environment created for data science and computational biology.



## Building the container Jupyter Server

    docker build -t compbio-research/jupyteraire:0.5.0 /base/my/script2/jupyteraire



## Running the Jupyter server

### Access controlled by password

From within Python 3, create a password hash

    In [1]: from notebook.auth import passwd
    In [2]: passwd()
    Enter password:
    Verify password:
    Out[2]: 'sha1:06508b76a41d:a6881e25fc21e92a4e0e6701e50357f23635ed39'

The password is required by the JupyterLab service to grant user access.


### Encrypted access to the service

Create a self-signed certificate valid for 2 years

    openssl req -x509 -nodes -days 730 -newkey rsa:2048 \
    -keyout /home/igregore/.aws/aire01.key -out /home/igregore/.aws/aire01.pem

The certificate is required by the user's browser to authenticate the service.


### Run the Jupyter server as a daemon

Run the service protected by password and through an encrypted connection

    docker run -d --rm \
    -v /home/igregore/Downloads:/home/igregore/Downloads \
    -v /home/igregore/.aws:/home/igregore/.aws \
    -p 8888:8888 -p 8786:8786 -p 8787:8787 \
    -u 6799:6799 -w / --name jupyteraire compbio-research/jupyteraire:0.5.0 jupyter lab \
    --port=8888 --no-browser --ip=0.0.0.0 \
    --certfile=/home/igregore/.aws/aire01.pem --keyfile=/home/igregore/.aws/aire01.key \
    --NotebookApp.password=sha1:06508b76a41d:a6881e25fc21e92a4e0e6701e50357f23635ed39

If the server is running on, for example,  machine 192.168.1.2, the service is
available at https://192.168.1.2:8888/lab. Port 8786 is used for communication
between the notebook and the dask scheduler. Port 8787 is used to serve details
of the scheduler as a webpage.

