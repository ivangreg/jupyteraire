## Use an official Fedora 31 runtime as a base image
FROM fedora:31
ENV container docker

# Note: hostname, mpich and mpich-devel are needed by MrBayes
#echo $'# Added by Ivan\nPATH=$PATH:/usr/lib64/mpich/bin\nexport PATH\n' >> /etc/profile;
#echo $'# Added by Ivan\nPATH=$PATH:/usr/lib64/mpich/bin\nexport PATH\n' >> /root/.bash_profile;
ENV PATH "PATH=$PATH:/usr/lib64/mpich/bin"
# Add Tini. Tini operates as a process subreaper for jupyter. This prevents
# kernel crashes.
ENV TINI_VERSION v0.6.0



# Author
LABEL maintainer="Ivan Gregoretti"



# Set the working directory to /opt
WORKDIR /opt



# Copy the current directory contents into the container at /opt
ADD . /opt
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /usr/bin/tini



# Create users

RUN useradd -m -u 1000 -d /home/fedora   -s /bin/bash -c "Fedora"           fedora   && usermod -a -G wheel fedora   && \
    useradd -m -u 6799 -d /home/igregore -s /bin/bash -c "Ivan Gregoretti"  igregore && usermod -a -G wheel igregore && \
    useradd -m -u 6829 -d /home/csimpson -s /bin/bash -c "Claire Simpson"   csimpson && usermod -a -G wheel csimpson && \
    useradd -m -u 6802 -d /home/ekolacz  -s /bin/bash -c "Elizabeth Kolacz" ekolacz  && \
    \
    mkdir /home/igregore/Downloads && chown -R igregore:igregore /home/igregore/Downloads && \
    mkdir /home/igregore/.aws      && chown -R igregore:igregore /home/igregore/.aws      && \
    \
    chmod +x /usr/bin/tini


# Current dnf Dask is installed with extras (dnf install python3-dask+dataframe)
# but in the future we should go even further (python3-dask+distributed) as
# library dependencies get resolved by the programming community. pip is still
# needed for "bokeh" and it could also provide "distributed".
#
# Also, this version of Dockefile removes python3-pandas from dnf and installs
# it from source. For that, dnf's python3-Cython is required.
#
# Also, python3-networkx requires python3-pandas and python3-decorator
# python3-pygraphviz python3-pydot python3-pyyaml python3-gdal. I will be
# installing this dependencies (except python3-pandas) and then installing
# networkx from zip archive.
#
# Also, separatelly install weak dependencies for python3-biopython (except
# phyton3-pandas and phyton3-pandas-datareader.
#
# Removing python3-dask+dataframe from dnf's job. Installing its dnf dependencies:
# python3-blosc python3-cloudpickle python3-locket python3-partd python3-toolz
#
# If BiocManager is not found, replace https://cloud.r-project.org by
# https://cran.case.edu

RUN sed -i -e "\$afastestmirror=true" /etc/dnf/dnf.conf && \
dnf install -y --nogpgcheck \
http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-31.noarch.rpm \
http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-31.noarch.rpm && \
dnf install -y dnf-plugins-core redhat-rpm-config && \
dnf clean all \
\
\
&& dnf update -y && dnf clean all \
\
\
&& dnf install -y \
sudo exa vim htop iftop iotop nethogs collectl pigz pbzip2 \
s3fs-fuse fuse-sshfs fuse-encfs nfs-utils bindfs \
xorg-x11-fonts-75dpi xorg-x11-fonts-100dpi \
git bash-completion nodejs \
gcc gcc-c++ gcc-gfortran \
python3-devel python3-docopt python3-psutil \
jq python3-csvkit \
\
\
automake fuse-devel gcc-c++ libcurl-devel libssh2-devel libxml2-devel make openssl-devel \
\
\
EMBOSS clustal-omega \
hostname mpich mpich-devel \
python3-tkinter \
python3-matplotlib texlive-type1cm \
python3-sqlalchemy python3-mmtf python3-decorator python3-pygraphviz python3-pydot python3-pyyaml python3-gdal python3-scikit-learn \
python3-ipython python3-magic python3-PyQt4 python3-joblib python3-Cython python3-h5py python3-requests-ftp \
lzo python3-Bottleneck python3-atomicwrites python3-husl python3-more-itertools python3-nose python3-numexpr python3-patsy python3-pytest python3-requests-file python3-tables python3-wrapt python3-xlwt \
python3-neo4j-driver \
&& dnf clean all \
\
\
&& dnf install -y python3-beautifulsoup4 python3-cssselect python3-html5lib python3-netifaces \
&& dnf clean all \
&& dnf --setopt=install_weak_deps=False install -y python3-ipyparallel \
&& dnf clean all \
\
\
&& dnf install -y xdg-utils \
&& dnf clean all \
&& dnf --setopt=install_weak_deps=False install -y python3-biopython \
&& dnf clean all \
\
\
&& dnf install -y python3-mpi4py-mpich \
python3-setuptools python3-tox python3-sphinx python3-pip \
\
\
python3-notebook python3-tzlocal \
python3-paramiko graphviz python3-graphviz \
&& dnf clean all \
\
\
&& dnf --setopt=install_weak_deps=False install -y python3-blosc python3-cloudpickle python3-locket python3-partd python3-toolz \
&& dnf clean all \
\
\
&& dnf builddep -y --allowerasing python3-pandas \
&& dnf clean all \
\
\
&& dnf builddep -y --allowerasing R.x86_64 \
&& dnf clean all \
&& dnf install -y \
redhat-rpm-config \
mesa-libGLU-devel \
libxml2-devel \
mariadb-devel mariadb \
java-1.8.0-openjdk-devel \
texinfo \
texinfo-tex \
texlive-tex \
texlive-latex \
texlive-latex-bin* \
texlive-makeindex-bin* \
texlive-inconsolata \
texlive-kpathsea-lib-devel \
gcc gcc-gfortran \
readline-devel \
libICE-devel \
libXt-devel \
libjpeg-turbo-devel \
libtiff-devel \
cairo-devel \
libunwind-devel zeromq-devel \
&&  dnf clean all \
\
\
&& sed -i -e "\$a# Added by Ivan\nPATH=$PATH:/usr/lib64/mpich/bin" /etc/profile && \
sed -i -e "\$a# Added by Ivan\nPATH=$PATH:/usr/lib64/mpich/binn\nexport PATH" /etc/profile && \
cd /opt && tar zxvf /opt/mrbayes-3.2.6.tar.gz  && chown -R igregore:igregore /opt/mrbayes-3.2.6* && \
runuser -l igregore -c 'cd /opt/mrbayes-3.2.6/src; autoconf; PATH=$PATH:/usr/lib64/mpich/bin; ./configure --enable-mpi --with-beagle=no; make' && \
\
\
curl 'https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip' -o 'awscliv2.zip' && \
unzip awscliv2.zip && \
./aws/install && \
sed -i '$ a complete -C "/usr/local/bin/aws_completer" aws' /home/igregore/.bashrc && \
\
\
bash -c "(\
    cd /opt && \
    tar zxvf /opt/R-3.6.2.tar.gz && \
    chown -R igregore:igregore /opt/R-3.6.2* && \
    cd /opt/R-3.6.2/ && \
    ./configure --enable-R-shlib && \
    make && \
    make check && \
    make pdf && \
    make info && \
    make install && \
    make install-info && \
    make install-pdf && \
    /usr/local/bin/R CMD javareconf && \
    echo \"if (!requireNamespace('BiocManager')){install.packages('BiocManager', repos='https://cran.case.edu')};BiocManager::install(  c('repr', 'IRdisplay', 'evaluate', 'crayon', 'pbdZMQ', 'devtools', 'uuid', 'digest', 'ShortRead', 'RMySQL', 'sqldf', 'neo4r', 'stringr', 'stringdist', 'corrplot', 'magrittr', 'dplyr', 'data.table', 'ggplot2', 'scales', 'Cairo', 'futile.logger', 'caret', 'tensorflow', 'keras', 'glmnet', 'shiny', 'genefilter', 'RJDBC', 'docopt'), ask=FALSE  )\" \
    | /usr/local/bin/R --slave --args /dev/stdin && \
    echo \"devtools::install_github('IRkernel/IRkernel');IRkernel::installspec(user=FALSE)\" \
    | /usr/local/bin/R --slave --args /dev/stdin)" && \
\
\
setcap "cap_net_admin,cap_net_raw+pe" /usr/sbin/nethogs && \
chmod 640 /etc/sudoers && sed --in-place -e 's|#\s*\(%wheel\s*ALL=(ALL)\s*NOPASSWD:\s*ALL\)|\1\nDefaults lecture=never|' /etc/sudoers && chmod 440 /etc/sudoers && \
sed --in-place -e 's|#\s*user_allow_other|user_allow_other|' /etc/fuse.conf && \
\
\
touch /etc/ld.so.conf.d/my-R.conf && chmod 644 /etc/ld.so.conf.d/my-R.conf && \
bash -c "(echo '/usr/local/lib64/R/lib' > /etc/ld.so.conf.d/my-R.conf)" && ldconfig -v && \
\
\
pip install --upgrade pip && \
pip install --upgrade setuptools && \
pip install \
    boto3 \
    pandas statsmodels pandas-datareader seaborn networkx \
    pymc3 arviz ggplot pybigwig weblogo biomart \
    bokeh \
    rpy2 rpy2-bioconductor-extensions rtools biomartpy \
    tensorflow==2.1.0 scipy==1.4.1 \
    Keras virtualenv \
    jupyterlab==1.2.4 "tornado!=6.0.0,!=6.0.1,!=6.0.2" \
    jupyterlab_latex ipysheet \
    dask distributed dask[dataframe] pyarrow "fsspec>=0.3.3" \
    gremlinpython && \
jupyter labextension install jupyterlab_vim jupyterlab-drawio @jupyterlab/toc @jupyterlab/fasta-extension && \
jupyter labextension install @jupyter-widgets/jupyterlab-manager && \
jupyter labextension install ipysheet && \
npm install -g json2csv



# Re-set the working directory to something more convenient
WORKDIR /home/igregore



ENTRYPOINT ["/usr/bin/tini", "--"]



EXPOSE 8888



CMD ["jupyter", "lab", "--port=8888", "--no-browser", "--ip=0.0.0.0"]

